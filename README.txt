
Overview of optin.module 
------------------------

Users who sign up to receive or do something have "opted in". For
example, a user who checks a box labeled "I want to receive news about
ACME Products" has opted in to receive email on that topic. Email to
users who have opt-ed in is, by definition, not SPAM. Opt-in e-mail has
been endorsed as the best practice for marketers by the Internet Direct
Marketing Bureau (IDMB).

This module provides a mechanism for users to opt-in for multiple
things. They simply check the desired boxes in their account profile or
when they are registering with this web site. Cron periodicly checks to
see who has opted in and grants a special role to those users.

With other modules, email can then be sent to everyone in a particular
role, and access to content can also be restricted based on roles.


Installation 
------------

1) Install the MySQL tables: mysql -u drupal -p drupal < optin.mysql

2) Put optin.module in your drupal module directory.

3) Go to admin/settings/optin to configure the cron setting.

4) Go to admin/user/configure/optin to setup the available opt-ins.


Requirements 
------------

cron and profile.module


Author 
------

Nic Ivy (nji@njivy.org)
