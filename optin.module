<?php

/**
 * Implementation of hook_help()
 */
function optin_help($section) {
  if (arg(5)) {
    if (is_numeric(arg(5)) && $section == 'admin/user/configure/optin/edit/'.arg(5)) {
      return t('NOTE: If more than one opt-in grants the same role, the user must select all of the options to be opted-in.');
    }
  }

  switch ($section) {
    case 'admin/modules#description':
      return t('Grant roles to users who opt-in.');
    case 'admin/help#optin':
      return t('<p>Users who sign up to receive or do something have <em>opted in</em>.  For example, a user who checks a box labeled "I want to receive news about ACME Products" has opted in to receive email on that topic.  Email to users who have opted in is, by definition, not SPAM.</p>
<p>Opt-in e-mail has been endorsed as the best practice for marketers by the Internet Direct Marketing Bureau (IDMB).</p>
<p>This module provides a mechanism for users to opt-in for multiple things.  They simply check the desired boxes in their account profile or when they are registering with this web site.  Cron periodicly checks to see who has opted in and grants a special role to those users. Email can then be sent to everyone in a particular role, and access to content can also be restricted based on roles.</p>
<p>NOTE: Both cron and profile.module are required for this module.  Check %a for modules that send email and restrict access.</p>', array('%a'=>l('Drupal.org', 'http://drupal.org/project/release')));
    case 'admin/user/configure/optin':
      return t('Use this area to create multiple opt-in options.  Specify which existing profile checkbox to use, what role to grant to people who opt-in, and what kinds of users have the option of opting-in.  Access controls are based on user roles.<br />
<br />
NOTE: Both cron and profile.module are required.');
  }
}

/**
 * Implementation of hook_menu()
 */
function optin_menu($may_cache) {
  $items = array();

  if ($may_cache) {
    $items[] = array('path' => 'admin/user/configure/optin',
                     'title' => t('opt-in'),
                     'callback' => 'optin_admin',
                     'type' => MENU_LOCAL_TASK,
                     'access' => user_access('administer users') || user_access('administer opt-ins')
                    );
  }

  return $items;
}

/**
 * Implementation of hook_perm()
 */
function optin_perm() {
  return array('administer opt-ins');
}

/**
 * Implementation of hook_settings()
 */
function optin_settings() {
  return form_textfield(t('Hours between cron updates'), 'optin_cron_period', variable_get('optin_cron_period', 12), 3, 10, t('Specify the minimum number of hours between updates.'));
}

/**
 * Implementation of hook_cron()
 */
function optin_cron() {
  $cron_period = variable_get('optin_cron_period', 12); // Units of hours
  $cron_last_run = variable_get('optin_cron_last_run', 0); // Units of seconds
  $now = time();

  // 5-sec fudge factor
  if ($now - $cron_last_run + 5 > ($cron_period * 3600)) {
    $o_result = db_query('SELECT * FROM {optin}');

    // Look at each opt-in.
    $grants = array();
    while ($optin = db_fetch_object($o_result)) {
      if ($optin->role_required) {
        $u_result = db_query('SELECT ur.uid, pv.value FROM {users_roles} ur LEFT JOIN {profile_values} pv ON ur.uid=pv.uid LEFT JOIN {profile_fields} pf ON pv.fid=pf.fid WHERE pv.fid=%d AND ur.rid=%d', $optin->fid, $optin->role_required);
      }
      else {
        $u_result = db_query('SELECT u.uid, pv.value FROM {users} u LEFT JOIN {profile_values} pv ON u.uid=pv.uid LEFT JOIN {profile_fields} pf ON pv.fid=pf.fid WHERE pv.fid=%d', $optin->fid);
      }

      // Find who opted-in and who did not.
      while ($user = db_fetch_object($u_result)) {
        if ($user->value) {
          // If the role was already set and this value is asserted, keep the old value.  Net effect is a logical AND.
          if (!isset($grants[$user->uid][$optin->role_granted])) {
            $grants[$user->uid][$optin->role_granted] = 1;
          }
        }
        else {
          // If this value is 0, we don't care what value was set before. Net effect is a logical AND.
          $grants[$user->uid][$optin->role_granted] = 0;
        }
      }
    }

    // Do the granting now.
    foreach ($grants AS $uid=>$roles) {
      $user = user_load(array('uid'=>$uid));
      $roles = $user->roles;

      foreach ($grants[$uid] AS $affected_role=>$grant) {
        if ($grant) {
          $roles[$affected_role] = 1;
        }
        else {
          unset($roles[$affected_role]);
        }
      }
      user_save($user, array('roles'=>$roles));
    }

    variable_set('optin_cron_last_run', $now);
  }
}

/**
 * Opt-in administration
 */
function optin_admin() {
  $op = $_POST['op'];
  $edit = $_POST['edit'];
  $id = arg(5);

  if (empty($op)) {
    $op = arg(4);
  }

  switch ($op) {
    case t('edit'):
      //
      // Display the form to edit a single periodical.
      //
      if (!is_numeric($id)) {
        drupal_set_message(t('Invalid id.'), 'error');
        drupal_goto('admin/user/configure/optin');
        break;
      }

      $optin = db_fetch_object(db_query('SELECT * FROM {optin} WHERE optin_id=%d', $id));

      // Pass '1' to exclude 'anonymous user'.
      $roles = user_roles(1);

      $result = db_query('SELECT fid, category, title FROM {profile_fields} WHERE type="checkbox" ORDER BY category, title');
      
      if (db_num_rows($result)) {
        $checkboxes = array();
        while ($row = db_fetch_object($result)) {
          $checkboxes[$row->fid] = $row->category.': '.$row->title;
        }
      }
      else {
        drupal_set_message(t('Please create a checkbox to use in the opt-in process.  When finished, return to finish editing the opt-in.'), 'error');
        drupal_goto('admin/user/configure/profile');
        break;
      }

      $form = form_textfield(t('Name'), 'name', $optin->name, 64, 64);
      $form .= form_select(t('Role to grant'), 'role_granted', $optin->role_granted, $roles, t('When a user opts-in, they will be granted this role and the permissions associated with it.'));
      $form .= form_select(t('Required role'), 'role_required', $optin->role_required, array('0'=>'<'.t('none').'>')+$roles, t('Optionally choose a role that a user must have before they can opt-in.'));
      $form .= form_select(t('Checkbox'), 'fid', $optin->fid, $checkboxes, t('Which checkbox will cause the user to opt-in?'));
      $form .= form_submit(t('Save opt-in'));
      $form .= form_submit(t('Delete opt-in'));
      $form .= form_hidden('optin_id', $optin->optin_id);

      $output = form($form);

      print theme('page', $output);
      break;

    case t('Add opt-in'):
      $edit['optin_id'] = db_next_id('{optin}_id');

      db_query('INSERT INTO {optin} (optin_id, name, fid, role_granted, role_required)
                            VALUES (%d, "%s", -1, -1, -1)',
                            $edit['optin_id'], $edit['name']);

      drupal_set_message(t('Opt-in <em>%a</em> was created. Click %b to finish defining it.', array('%a'=>$edit['name'], '%b'=>l(t('edit'), 'admin/user/configure/optin/edit/'.$edit['optin_id']))));
      drupal_goto('admin/user/configure/optin');
      break;

    case t('Delete opt-in'):
      db_query('DELETE FROM {optin} WHERE optin_id=%d', $id);

      drupal_set_message(t('The opt-in was deleted.'));
      drupal_goto('admin/user/configure/optin');
      break;

    case t('Save opt-in'):
      db_query('UPDATE {optin} SET name="%s", fid=%d, role_granted=%d, role_required=%d WHERE optin_id=%d',
               $edit['name'], $edit['fid'], $edit['role_granted'], $edit['role_required'], $edit['optin_id']);

      drupal_set_message(t('Your changes were saved.'));
      drupal_goto('admin/user/configure/optin');
      break;

    default:
      //
      // Create a list of opt-ins with 'edit' links and a box to add a new one.
      //
      $result = db_query('SELECT optin_id AS id, name FROM {optin} ORDER BY name');

      $header = array(t('Name'), t('Operations'));
      while ($row = db_fetch_object($result)) {
        $rows[] = array($row->name, array('data' => l(t('edit'), 'admin/user/configure/optin/edit/'.$row->id), 'align' => 'center'));
      }
      $rows[] = array('<input type="text" size="64" maxlength="64" name="edit[name]" />', '<input type="submit" name="op" value="'. t('Add opt-in') .'" />');

      $output = theme('table', $header, $rows);
      $output = form($output);

      print theme('page', $output);
  }
}


?>
